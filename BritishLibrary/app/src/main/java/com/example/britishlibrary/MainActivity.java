package com.example.britishlibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPref; //In the class definition

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sharedPref = this.getPreferences(Context.MODE_PRIVATE); //In the onCreate method
        ((EditText)findViewById(R.id.borrowerID)).setText(sharedPref.getString("borrower_id","No ID set."));
    }

    public void setBorrowerIDClick(View view) {
        EditText bID = (EditText)findViewById(R.id.borrowerID);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("borrower_id", bID.getText().toString());
        editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}