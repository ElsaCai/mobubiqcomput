package cs.bham.ac.uk.assignment3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class FoodItem<getRequest> extends AppCompatActivity {
    private ArrayList<ItemList> products = new ArrayList<ItemList>();
    private FoodAdapter productsAdapt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_item);
        productsAdapt = new FoodAdapter( products);
        //RecyclerView listView = (RecyclerView) findViewById(R.id.productList);
        //((BottomNavigationView)findViewById(R.id.bottom_menu)).setOnNavigationItemSelectedListener((BottomNavigationView.OnNavigationItemSelectedListener) this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //listView.setLayoutManager(layoutManager);

        //listView.setAdapter(productsAdapt);
    }

    public void onRequestProducts(View view){
        Log.i("Elsa test", "Elsa");
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, "https://www.sjjg.uk./eat/food-items",null,
            new Response.Listener<JSONArray>()
            {
                @Override
                public void onResponse(JSONArray response)
                {
                    Log.i("Response",response.toString());
                    populateList(response);
                }
            },
            new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error) {Log.i("error", "error=="+ error.getMessage());}
            }
        );
        requestQueue.add(getRequest);
    }

    private void populateList(JSONArray items) {
        products.clear();
        try{
            for (int i =0; i<items.length();i++) {
                JSONObject jo = items.getJSONObject(i);
                products.add(new ItemList(jo.getString("name"),jo.getInt("id"),jo.getString("meal"),jo.getInt("time")));
            }
        }
        catch(JSONException err){}
        productsAdapt.notifyDataSetChanged();
    }
}
