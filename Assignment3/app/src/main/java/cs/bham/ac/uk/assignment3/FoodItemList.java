package cs.bham.ac.uk.assignment3;
import android.content.Intent;
public class FoodItemList {
    private int time;
    private String meal;
    private int id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FoodItemList(String name, int id, String meal, int time) {
        this.name = name;
        this.id = id;
        this.meal = meal;
        this.time = time;
    }

    public String toString(){
        return this.name;
    }

    public Integer getApiID() {
        return id;
    }

    public void setApiID(Integer id) {
        this.id = id;
    }
}
