package cs.bham.ac.uk.assignment3;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import com.google.android.material.bottomnavigation.BottomNavigationView;
/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        //final View button = view.findViewById(R.id.btn);
        Log.i("onCreateView", "onCreateView");
        //button.setOnClickListener(this);
        /*button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.i("Elsa test", "Elsa");
                        //RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, "https://www.sjjg.uk./eat/food-items",null,
                                new Response.Listener<JSONArray>()
                                {
                                    @Override
                                    public void onResponse(JSONArray response)
                                    {
                                        Log.i("Response",response.toString());
                                        //populateList(response);
                                    }
                                },
                                new Response.ErrorListener()
                                {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {Log.i("error", "error=="+ error.getMessage());}
                                }
                        );
                    }
                }
        );*/
        return inflater.inflate(R.layout.fragment_home, container, false);
    }
/*
    @Override
    public void onClick(View view) {
        Log.i("Elsa test2 ", "Elsa 2");
        if (view.getId() == R.id.btn) {
            Log.i("all in ", "all in");
        }
    }

    public void onRequestProducts(View view){
        Log.i("Elsa test", "Elsa");
        //RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, "https://www.sjjg.uk./eat/food-items",null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        Log.i("Response",response.toString());
                        //populateList(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {Log.i("error", "error=="+ error.getMessage());}
                }
        );
        //requestQueue.add(getRequest);
    }*/


}
