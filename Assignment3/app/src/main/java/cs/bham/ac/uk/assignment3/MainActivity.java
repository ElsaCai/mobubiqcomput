package cs.bham.ac.uk.assignment3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private ArrayList<FoodItemList> products = new ArrayList<FoodItemList>();
    private FoodAdapter productsAdapt;

    private SharedPreferences sharedPref;
    AboutFragment aF = new AboutFragment();
    SettingsFragment sF = new SettingsFragment();
    HomeFragment hF = new HomeFragment();
    FragmentManager fm = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        productsAdapt = new FoodAdapter( products);
        RecyclerView listView = (RecyclerView) findViewById(R.id.productList);
        //((BottomNavigationView)findViewById(R.id.bottom_navi)).setOnNavigationItemSelectedListener((BottomNavigationView.OnNavigationItemSelectedListener) this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(layoutManager);
        listView.setAdapter(productsAdapt);

        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        ((EditText)findViewById(R.id.borrowerID)).setText(sharedPref.getString("borrower_id","No ID set."));
        ((BottomNavigationView)findViewById(R.id.bottom_navi)).setOnNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            fm.beginTransaction().add(R.id.frag_frame, hF).commit();
        }
    }

    public void setBorrowerIDClick(View view) {
        Log.i("setBorrowerIDClick", "setBorrowerIDClick");
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, "https://www.sjjg.uk./eat/food-items",null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        Log.i("Response",response.toString());
                        populateList(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {Log.i("error", "error=="+ error.getMessage());}
                }
        );
        requestQueue.add(getRequest);
        /*EditText bID = (EditText) findViewById(R.id.borrowerID);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("borrower_id", bID.getText().toString());
        editor.commit();
         */
    }

    private void populateList(JSONArray items) {
        products.clear();
        try{
            for (int i =0; i<items.length();i++) {
                JSONObject jo = items.getJSONObject(i);
                products.add(new FoodItemList(jo.getString("name"),jo.getInt("id"),jo.getString("meal"),jo.getInt("time")));
            }
        }
        catch(JSONException err){}
        productsAdapt.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
                Intent i = new Intent(MainActivity.this, OptionsActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        FragmentTransaction fT = fm.beginTransaction();
        switch (item.getItemId())
        {
            case R.id.home:
                fT.replace(R.id.frag_frame, hF);
                break;
            case R.id.settings:
                fT.replace(R.id.frag_frame, sF);
                break;
            case R.id.about:
                fT.replace(R.id.frag_frame, aF);
                break;
        }
        fT.commit();
        return true;
    }
}
