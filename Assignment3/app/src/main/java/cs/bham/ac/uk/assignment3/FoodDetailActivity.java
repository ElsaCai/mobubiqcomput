package cs.bham.ac.uk.assignment3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class FoodDetailActivity extends AppCompatActivity {
    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);
        id = getIntent().getIntExtra("id", this.id);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, "https://www.sjjg.uk/eat/recipe-details/" + id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("Response",response.toString());
                        TextView description = findViewById(R.id.description);
                        TextView ingredients = findViewById(R.id.ingredients);
                        TextView steps = findViewById(R.id.steps);
                        try {
                            //name.setText(response.getString("name"));
                            description.setText(response.getString("description"));
                            ingredients.setText(response.getString("ingredients"));
                            steps.setText(response.getString("steps"));
                            Log.i("description",response.getString("description"));
                            Log.i("ingredients",response.getString("ingredients"));
                        } catch (JSONException err) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("error", "error=="+ error.getMessage());
                    }
                }
        );
        requestQueue.add(getRequest);
    }

    public void Add(View view) {
        Log.i("Add","Add");
    }
}
